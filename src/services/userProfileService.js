const { UsersModel } = require('../models/userModel')
const bcrypt = require('bcrypt')

const getUserProfile = async(userId) => {
    const user = await UsersModel.findOne({ _id: userId })
    return {
        user: { _id: user._id, username: user.username, createdDate: user.createdDate }

    }
}
const deleteUserProfile = async(userId) => {
    const user = await UsersModel.findOneAndDelete({ _id: userId })
}
const changeUserPass = async(userId, oldPassword, newPassword) => {
    console.log('bew pass ++++++++++++++++++++++++++++++++++', newPassword)

    const user = await UsersModel.findOne({ _id: userId })
    if (!(await bcrypt.compare(oldPassword, user.password))) {
        const err = new Error("Old password is incorrect");
        err.status = 400
        throw err
    }
    await UsersModel.updateOne({ _id: userId }, { $set: { password: await bcrypt.hash(newPassword, 10) } })

}

module.exports = {
    getUserProfile,
    deleteUserProfile,
    changeUserPass

}