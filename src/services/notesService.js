const { NotesModel } = require('../models/noteModel')
const { UsersModel } = require('../models/userModel')

const getUserNotes = async({ _id, offset, limit }) => {
    const NOTES_PER_QUERY_MAX = 10
    offset = Number(offset)
    limit = NOTES_PER_QUERY_MAX > Number(limit) ? Number(limit) : NOTES_PER_QUERY_MAX
    const count = await NotesModel.find({ userId: _id }).countDocuments()
    const notes = await NotesModel.find({ userId: _id }).skip(offset).limit(limit)
    return { notes, count }
}
const createNoteForUser = async(userId, text) => {
    const user = await UsersModel.findOne({ _id: userId })
    const note = new NotesModel({
        text: text,
        userId: user._id
    })
    await note.save()
}
const getNoteById = async(noteId) => {
    const note = await NotesModel.findById(noteId)
    return note
}
const updateNoteById = async(noteId, text) => {
    const filter = { _id: noteId }
    const update = { text }
    await NotesModel.findOneAndUpdate(filter, update)
}
const patchNoteById = async(noteId) => {
    const note = await getNoteById(noteId)
    note.completed = !note.completed
    note.save()
}
const deleteNoteById = async(noteId) => {
    await NotesModel.findByIdAndDelete(noteId)
}

const checkNoteOwnership = async(req, res, next) => {
    const userId = req.user._id
    const noteId = req.params.id
    const note = await NotesModel.findById(noteId)
    if (!note) {
        const err = new Error('no such post')
        err.status = 400
        throw err
    }
    const noteUserId = String(note.userId)
    if (!(noteUserId === userId)) {
        const err = new Error('Have no permission123123')
        err.status = 400
        throw err
    }
}
module.exports = {
    getUserNotes,
    createNoteForUser,
    getNoteById,
    checkNoteOwnership,
    updateNoteById,
    patchNoteById,
    deleteNoteById
}