const bcrypt = require('bcrypt')
var jwt = require('jsonwebtoken')

const { LoginErrors } = require('../utils/errors')
const { UsersModel } = require('../models/userModel')

const registration = async({ username, password }) => {
    const user = new UsersModel({
        username,
        password: await bcrypt.hash(password, 10)
    })
    await user.save()

}
const signIn = async({ username, password }) => {
    const user = await UsersModel.findOne({ username })
    if (!user) {
        throw new LoginErrors();
    }
    if (!(await bcrypt.compare(password, user.password))) {
        throw new Error('Invalid email or password');
    }
    const token = jwt.sign({
        _id: user._id,
        username: user.username,
    }, 'key')
    return token
}
module.exports = {
    registration,
    signIn
}