const mongoose = require('mongoose');

const logSchema = new mongoose.Schema({
    log: String
})
const logger = mongoose.model('logger', logSchema)

module.exports = {
    logger
}