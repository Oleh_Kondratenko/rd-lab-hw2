const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    completed: {
        type: Boolean,
        default: false
    },
    text: String,
    createdDate: {
        type: Date,
        default: Date.now()
    }
})
const NotesModel = mongoose.model('Notes', noteSchema)

module.exports = {
    NotesModel
}