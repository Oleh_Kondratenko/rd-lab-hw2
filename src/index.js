const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const { NotesAppErrors } = require('./utils/errors');
const { authRouter } = require('./controllers/authController');
const { notesRouter } = require('./controllers/notesController');
const { usersRouter } = require('./controllers/usersController');
const { authMiddleware } = require('./middlewares/authMiddleware');

const PORT = 8080;

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/users/me', [authMiddleware], usersRouter);
app.use('/api/notes', [authMiddleware], notesRouter);
app.use('/api/auth', authRouter);

app.use((req, res, next) => {
    res.status(404).json({ message: 'Not found' });
});

app.use(function(err, req, res, next) {
    if (err instanceof NotesAppErrors) {
        return res.status(400).json({ message: err.message });
    }
    res.status(500).json({ message: err.message });
});

const start = async() => {
    try {
        await mongoose.connect('mongodb+srv://Oleh:Qwerty123@cluster0.1pnci.mongodb.net/HW2?retryWrites=true&w=majority', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        app.listen(PORT);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

start();