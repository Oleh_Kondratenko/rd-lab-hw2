class NotesAppErrors extends Error {
    constructor(message) {
        super(message)
        this.status = 500;
    }
}
class RegisterErrors extends NotesAppErrors {
    constructor(message) {
        super(message)
        this.status = 400;
    }
}
class LoginErrors extends NotesAppErrors {
    constructor(message = 'Wrong login or pass') {
        super(message)
        this.status = 400;
    }
}
module.exports = {
    NotesAppErrors,
    RegisterErrors,
    LoginErrors
}