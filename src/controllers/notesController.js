const express = require('express')
const router = express.Router()
const {
    getUserNotes,
    createNoteForUser,
    getNoteById,
    checkNoteOwnership,
    updateNoteById,
    patchNoteById,
    deleteNoteById
} = require('../services/notesService')
const { asyncWrapper } = require('../utils/apiUtils')


router.get('/', asyncWrapper(async(req, res, next) => {
    const { offset = 0, limit = 10 } = req.query
    const { _id } = req.user
    const { notes, count } = await getUserNotes({ _id, offset, limit });
    res.status(200).json({
        "offset": offset,
        "limit": limit,
        "count": count,
        "notes": notes
    })
}))
router.post('/', asyncWrapper(async(req, res, next) => {
    const { _id } = req.user
    const { text } = req.body
    await createNoteForUser(_id, text)
    res.status(200).json({ message: "Success" })
}))

router.get('/:id', asyncWrapper(async(req, res, next) => {
    const noteId = req.params.id
    await checkNoteOwnership(req, res, next)
    const note = await getNoteById(noteId)
    res.status(200).json({ "note": note })
}))

router.put('/:id', asyncWrapper(async(req, res, next) => {
    const { text } = req.body
    const noteId = req.params.id
    await checkNoteOwnership(req, res, next)
    await updateNoteById(noteId, text)
    res.status(200).json({ message: "Success" })
}))

router.patch('/:id', asyncWrapper(async(req, res, next) => {
    const noteId = req.params.id
    await patchNoteById(noteId)
    res.status(200).json({ message: "Patched" })
}))

router.delete('/:id', asyncWrapper(async(req, res, next) => {
    const noteId = req.params.id
    await checkNoteOwnership(req, res, next)
    await deleteNoteById(noteId)
    res.status(200).json({ message: "Deleted" })
}))

module.exports = {
    notesRouter: router
}