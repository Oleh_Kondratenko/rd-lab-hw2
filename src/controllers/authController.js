const express = require('express');
const router = express.Router();

const {
    registration,
    signIn
} = require("../services/authService")

const {
    asyncWrapper
} = require('../utils/apiUtils')

const RESDELAY = 0

router.post('/register', asyncWrapper(async(req, res, next) => {
    const { username, password } = req.body

    await registration({ username, password })
    res.status(200).json({ message: "Success" })

}))
router.post('/login', asyncWrapper(async(req, res, next) => {
    const {
        username,
        password
    } = req.body

    const token = await signIn({ username, password })

    res.status(200).json({
        "message": "Success",
        "jwt_token": token
    })
}))


module.exports = {
    authRouter: router
}