const express = require('express');
const router = express.Router();
const { asyncWrapper } = require('../utils/apiUtils')
const { getUserProfile, deleteUserProfile, changeUserPass } = require('../services/userProfileService')


router.get('/', asyncWrapper(async(req, res, next) => {
    const userId = req.user._id
    const userInfo = await getUserProfile(userId)
    res.status(200).json({...userInfo })
}))
router.delete('/', asyncWrapper(async(req, res, next) => {
    const userId = req.user._id
    await deleteUserProfile(userId)
    res.status(200).json({ message: "account deleted" })
}))
router.patch('/', asyncWrapper(async(req, res, next) => {
    const userId = req.user._id
    const { oldPassword, newPassword } = req.body
    await changeUserPass(userId, oldPassword, newPassword)
    res.status(200).json({ message: "password changed" })
}))


module.exports = {
    usersRouter: router
}